#!/usr/bin/env pythoh3

'''
Programa para mostrar la lista de la compra
'''

def main():
    habitual: tuple = ("patatas", "leche", "pan")
    especifica: list = []
    especifica_rep: list = []
    seguir = True
    while seguir:
        print("Elemento a comprar: ", end="")
        comida = str(input())
        if comida != "":
            especifica_rep.append(comida)
        if comida != "" and comida not in habitual and comida not in especifica:
            especifica.append(comida)
        elif comida == "":
            print("Lista de la compra: ", *habitual, sep="\n")
            print(*especifica, sep="\n")

            print("Elementos habituales:", len(habitual))
            print("Elementos específicos:", len(especifica_rep))
            print("Elementos en lista:", len(habitual) + len(especifica))

            seguir = False


if __name__ == '__main__':
    main()
